# **Version: 2.63.X**
  **Release: Production**
## Features
 1. **Events Report**  
     *Update*: `Controller`  
     Users can generate the report of the number of occurrences of specified events in the given time interval.
 2. **Persistent Rows per page**  
     *Update*: `Controller`  
     The Rows per page set on any page will apply to all other pages.
 3. **Artifact Description**  
     *Update*: `Controller`  
     A small description could be added to an artifact while uploading the artifact.
 4. **Save Reports and Get Daily reports**  
     *Update*: `Controller`  
     Users can save the report and get the reports daily as emails
## Note
 * The search input field is left aligned on the Devices and Reports pages
 * The Addition, edition and deletion of tags have been provided under the Manage Tags section.
## Bug Fixes
 * #2377- The Update statistics changes if the deployed artifact is deleted
 * #2312- Incorrect device deployment time is displayed
 * #2303- Previously executed offline commands are not displayed in the details page
 * #2373- There is no sync between the overall deployment status and the status column of the devices table on the deployment details page
## Known Caveats
 * #2430- User can access the "user-management" page even after changin the role
 * #2416- Rows per page and filters are not updated in the url

# **Version: 2.60.X**
  **Release: Production**
## Features
 1. **Update Statistics**  
     *Update*: `Controller`  
     Users can get to know how many devices tried receiving the partial/full artifact in a deployment. The count of devices on which the deployment of partial/full artifact was tried is updated on the deployment details page and could be seen by clicking the *View Details* button.
 2. **Export deployment of Devices**  
    *Update*: `Controller`  
      The devices table present on the deployment details page could be exported as a CSV file
 3. **Device Upgrade Summary Updated**  
     *Update*: `Controller`  
      Instead of updating the device upgrade summary with the artifact_name, the `deployed_image` tag will be used.
 4. **Command Execution By Reporter**  
     *Update*: `Controller`  
     Users with the *Reporter* role can execute the commands on the devices.
 5. **Set Port Timeout**  
    *Update*: `Controller`  
    While opening a port on the device, the user can decide on how long the port should be opened up. This could be done by clicking the dropdown arrow and then selecting the desired time limit.
## Bug Fixes
 * #2234- Pagination: The back button doesn't navigate to previous page
 * #2328- Business Telemetry function is not displayed on post editing the "Select report" module.
 * #2327- Incomplete digits are displayed on the y-axis scale of "Last successfully deployed artifacts" graph.
 * #2310- "pq: offset must not be negative " error message is displayed if the artifacts is searched and deleted
 * #2307- On post changing the date picker from "Last Week" to "Last Day" on previously executed commands page, Deployments page is displayed
 * #2301- Sub artifacts are not sorted
 * #2298- Artifacts are not sorted w.r.t Names
## Known Caveats
 * #2282- Process names over flow on the device profile's monitor page
 * #2386- Partial OTA fails on the very first deployment on a new device
 * #2384- Unable to map the exported files and the corresponding deployments
 * #2382- 0 devices received full update message is displayed on post using the offline command execution feature
 * #2377- The Update statistics changes if the deployed artifact is deleted
 * #2373- There is no sync between the overall deployment status and the status column of the devices table on the deployment details page
 * #2357- Unable to add a tag key having special character

# **Version: 2.58.X**
  **Release: Production**
## Features
 1. **Artifact Grouping**  
     *Update*: `Controller`  
     Artifacts having the same name will be grouped to make a Release artifact. Note: This will be appliable to only those artifact which would be uploaded after this release.
 2. **Partial OTA Upgrade**  
     *Update*: `Controller` and `Aiagent`  
     User can update their software with a diff file itself instead of upgrading the entire software.
 3. **Sign in using 2FA**  
     *Update*: `Controller`  
      User can sign in using the 2 Factor Authentication feature. This feature is configured on request.
 4. **Optimized Report**  
    *Update*: `Controller`  
    User need not wait for a longer time to see the report. Report generation will be quicker.
## Bug Fixes
 * #2243- Incorrect error message is displayed if duplicate device names are present in the uploaded CSV file
 * #2018- Incorrect error message is displayed if user tries to generate a business telemetry report without selecting the function
## Known Caveats
 * #2328- Business Telemetry function is not displayed on post editing the "Select report" module.
 * #2327- Incomplete digits are displayed on the y-axis scale of "Last successfully deployed artifacts" graph.
 * #2326- Incorrect error message when "Unable to regenerate the report" even after waiting for a long time.
 * #2312- Incorrect device deployment time is displayed
 * #2311- The "current artifact" gets updated before the deployment gets completed
 * #2310- "pq: offset must not be negative " error message is displayed if the artifacts is searched and deleted
 * #2307- On post changing the date picker from "Last Week" to "Last Day" on previously executed commands page, Deployments page is displayed
 * #2303- Previously executed offline commands are not displayed in the details page
 * #2301- Sub artifacts are not sorted
 * #2300- Tedious to get to know on which device profile an artifact is configured for an automated deployment
 * #2298- Artifacts are not sorted w.r.t Names

# **Version: 2.56.X**
  **Release: Production**
## Feature
 1. **Enable/disable the docker logs**  
      *Update*: `Controller` and `Aiagent`  
      The docker logs could be monitored on post enabling it in the monitoring section of Device Profile's configure page.
 2. **Set SSH Tunnel Timeout**  
      *Update*: `Controller`  
      The ssh tunnel timeout period could be set while getting the ssh command for a device. The timeout period could be 30,60,120 or 180 mins.
## Bug Fixes
 * #2187- No Confirmation modal appears for resetting a device on post tampering it
 * #2174- 500 Internal server error message is displayed if tried to generate a business telemetry report without selecting the function
 * #2159- Incorrect network and device uptime reports are generated if user tries to generate the report with less than an hour time interval
 * #2151- Unable to retry a deployment when created using a CSV file
 * #2147- The Installing filter disappears and the devices list is not displayed on deployment details page
 * #2146- Unable to identify which device is updating
## Known Caveats
 * #2243- Incorrect error message is displayed if duplicate device names are present in the uploaded CSV file 
 * #2234- Pagination: The back button doesn't navigate to previous page
 * #2233- On post deleting a device profile, the first page is displayed
# **Version: 2.53.X**
  **Release: Production**
## Feature
 1. **Improved Department Scopes**  
     *Update*: `Controller`  
     Multiple departments can have the same device profile as its scope.
  2. **Intrusion Detection**  
      *Update*: `Controller` and `Aiagent`  
      If an unknown user breaks the device case, an intrusion detection alarm would be raised on the device's info page, and the operator can either acknowledge the intrusion and move back the device to its normal state or can reset the device and make it a brick.
## Bug Fixes
 * #2140- Unable to select the time with custom date picker option on Reports page
## Known Caveats
 * #2189- On post rebooting the device, the Tamper sensor state changes to "Tampered" state from "Acknowledged" state
 * #2187- No Confirmation modal appears for resetting a device on post tampering it
 * #2174- 500 Internal server error message is displayed if tried to generate a business telemetry report without selecting the function
 * #2159- Incorrect network and device uptime reports are generated if user tries to generate the report with less than an hour time interval

# **Version: 2.52.X**
  **Release: Internal**
## Feature
 1. ** Export Devices**
     *Update*: `Controller`
     The devices present on the Devices page, Device Profile's devices page and in Dynamic Group's devices page could be exported as a CSV file.
  2. **OTA deployments on selected devices**
     *Update*: `Controller`
     The OTA deployments could be created on selected devices only. User can either select the devices or upload a CSV file having the device names on which the deployment should be created. Make sure to specify correct, non-repetitive device names on the CSV file.
## Bug Fixes
 * #1680- Even though the application is deployed on the DP, "No Application deployed" message is displayed in android device
 * #2091- Persistent Sort feature is missing in "Neighbours" page
 * #2081- The character counter in the "Add new command" modal doesn't get updated while entering the command
 * #2080- Even after clearing the search input field, the previously searched device name is displayed first in the Reports page
 * #2078- Selected reports disappear on post navigating to other pages
## Known Caveats
 * #2153- The CPU Load increases on Raspberry Pi Zero W where AiAgent is running
 * #2152- operators cannot use the artifact uploaded by the admin
 * #2151- Unable to retry a deployment when created using a CSV file
 * #2149- Incorrect error message is displayed if user uploads a csv file that has invalid device names
 * #2148- Incorrect device list is displayed on the deployment details if user creates a deployment using a csv file that has duplicate device names
 * #2147- The Installing filter disappears and the devices list is not displayed on deployment details page
 * #2146- Unable to identify which device is updating
 * #2142- Business metric names are not displayed in the Visualizations page
 * #2140- Unable to select the time with custom date picker option on Reports page

# **Version: 2.50.X**
  **Release: Production**  
## Feature
 **Persistent Filters and Sort list**  
     *Updated*: `Controller`  
     The filters added in the Devices page, Events page, and Deployments page do not disappear on post reloading or navigating to other pages. The sorted list remains as is even after reloading the page.
## Bug Fixes
 * #2036- Open Device Port provides a valid URL for an invalid entry
 * #2027- No suggestion message is displayed if an invalid port number is entered
 * #1996- No loading sign/message is displayed on post clicking the logout button
 * #1995- On changing the date, the business telemetry graph doesn't get updated with the set function
 * #1993- Reports table is broken
 * #1992- On post logging in Dashboard is not displayed
 * #1967- Incorrect error message is displayed if a non existing device name is entered on the Search in put field of Reports page
 * #1939- Unable to sort the "Last successfully deployed artifacts" table
 * #1819- All of a sudden login page gets displayed
## Known Caveats
 * #2091- Persistent Sort feature is missing in "Neighbours" page
 * #2090- Aikaan logo and Microsoft logos are not displayed in the SSO window
 * #2081- The character counter in the "Add new command" modal doesn't get updated while entering the command
 * #2080- Even after clearing the search input field, the previously searched device name is displayed first in the Reports page
 * #2079- Generated reports table is inconsistent
 * #2078- Selected reports disappear on post navigating to other pages
 * #2075- Even if the user is skipped a green tick is displayed along with a "Operation Success" message
 * #2074- Incorrect error message is displayed if a registered user's email ID is used to add him in another account
 * #2068- On post logout, the added filter is removed in Devices page
 * #2067- dashboard breaks in test.aikaan.io

# **Version: 2.48.X**
  **Release: Production**
## Feature
 1. **Generating Multiple Business Metric Reports**  
     *Updated*: `Controller`  
     User can select multiple business metrics with appropriate functions. Different reports could be selected multiple times by clicking on the *Add another report* button.
 2. **Offline Agent Installation**  
    *Updated*: `Controller` and `Aiagent`  
    User can install the Ai agent on an offline device. Once the device gets connected with the internet it appears as a new device on the dashboard.
## NOTE
   The offline agent installation is not supported on ARM non-android devices.
## Bug Fixes
* #1969- 12 hour and 24 hour formats are used in device's "Uptime" page
* #1966- The process names are not displayed in the column header of collapsed report for a device
* #1937- Inconsistent heading in Deployments sub tab of Device profile/ Dynamic Groups
* #1897- Unable to use a single quote in the description input field of the devices
## Known Caveats
* #2036- Open Device Port provides a valid URL for an invalid entry
* #2032- Same report is displayed multiple times
* #2030- The "Device management" feature is present on "User Settings" page
* #2027- No suggestion message is displayed if an invalid port number is entered
* #2019- Unable to select a set of business metric names with different functions in the reports page
* #2018- Incorrect error message is displayed if user tries to generate a business telemetry report without selecting the function
* #2017- On clicking a cell on the Uptime page, current day's bubble is highlighted on the Health page

# **Version: 2.47.X**
  **Release: Production**
## Feature
 * **Visualization Of Multiple Graphs**  
   *Updated*:`Controller`  
   Grafana dashboard has been integrated with the Aikaan dashboard and users can visualize multiple graphs and tables using *Grafana* interface. A new navigation tab **Visualize** helps in accessing the new dashboard.
## Bug Fixes
 * #1965- An error message is displayed before the search result appears in the Reports page
 * #1918- The page reloads twice on post logging out
## Known Caveats
 * #1997- Double redirections happen on clicking the logout button 
 * #1996- No loading sign/message is displayed on post clicking the logout button
 * #1995- On changing the date, the business telemetry graph doesn't get updated with the set function
 * #1993- Reports table is broken
 * #1992- On post logging in Dashboard is not displayed
 * #1989- Column name in the reports page for the metric screen-uptime is wrong
 * #1987- Optimize map loading in the Dashboard
 
# **Version: 2.46.X**
  **Release: Production**
## Feature
 1. **Multiple Processes and Interfaces reports**  
     *Updated*: `Controller`  
     Users can select multiple process names and interfaces while selecting the values for each process report and data/network related reports respectively.
  2. **Device Upgrade Summary**  
     *Updated*: `Controller`  
     The Last successfully updated images on the devices of a device profile are summarised in the details page. Users can get to know the count of devices running a particular image. The report could be visualized as a bar graph or could be downloaded as a CSV file.
  3. **Tags on Device Info sections**  
      *Updated*: `Controller`  
      Tags that are attached to a device could be viewed on the device's info section. If there are more than 3 tags, click on the *View all tags* button to look at all the tags.
  4. **Restricted User management Access**  
      *Updated*: `Controller`  
      Only *Admin* can access the *User management* page. Users with neither *Reporter* nor the *Operator* role have access to the *User management* page.
  5. **Business Telemetry Report**  
      *Update*: `Controller`  
      Reports of *Business Metrics* could be generated. Users can select desired metric and its appropriate function in the report selection modal.    
## NOTE:
 * The *Deploy Application* section has been moved from the device profile's *Details* page to the device profile's *Configure* Page.
 * *Confirm Password* input field has been removed and eye icon has been introduced in the *Accounts* section
## Bug Fixes
 * #1894- Business Events details displays error message
 * #1853- End time should not be less than start time
 * #1574- log file names are not displayed properly in the drop-down list
 * #1509- On post clicking the "Execute" button, the Command Selection input field is not cleared.
## Known Caveats
 * #1986- "Sum" function is not present in the Business Telemetry reports selection modal 
 * #1970- The user gets added even if incorrect mail ID is provided in the csv file
 * #1969- 12 hour and 24 hour formats are used in device's "Uptime" page  
 * #1968- Empty cells are present in the exported network data upload and download report
 * #1967- Incorrect error message is displayed if a non existing device name is entered on the Search in put field of Reports page
 * #1966-  The process names are not displayed in the column header of collapsed report for a device
 * #1965- An error message is displayed before the search result appears in the Reports page
 * #1963- Incorrect Process data consumption value is displayed in the Reports page
 * #1954- Inconsistent overall value is displayed in the generated report
 * #1951- Unable to remove the very first report
 * #1946- Support Multiple business telemetry in reports
 * #1941- Use a statically-linked wget binary for ARM devices
 * #1939- Unable to sort the "Last successfully deployed artifacts" table
 * #1937- Inconsistent heading in Deployments sub tab of Device profile/ Dynamic Groups 


# **Version: 2.44.X**
  **Release: Internal**
## Feature
  **Agent for System V init devices**  
   *Updated*: `Controller` and `Aiagent`  
    Users can install agent on devices which run on SystemV init (non systemd).  
## NOTE:
  * The navigation tab *Apps* has been renamed as *Containers*
  * Following features are no longer available
    * *Peripherals* feature on device details page
    * *SMS* and *Telemetry* configurations on the device profile's configure page
    * *Network Traffic* on the device details page and in device profile's configure page
## Bug Fixes
 * #1905- On post installation of agent on a custom install directory, an error message is displayed with respect to creation of "aiupgrade.log" directory
 * #1860- Device Uptime is lesser than the Network Uptime
 * #1858- Detailed report is not displayed for an offline device
 * #1857- Unable to type the report name in the input field
 * #1856- Duplicate hotspot events are logged
 * #1854- The Hotspot event's timestamp is incorrect
 * #1822- Exported Device Uptime report contains empty value for a powered off device
 * #1750- The Version command overflows out of the Process card in the Monitoring page
 * #1746- Incorrect Network usage graph is displayed
 * #1742- Incomplete Version is displayed in Reports page
 * #1720- On changing the port number, the "copy link" doesn't disappear
 * #1688- "Number of Retries" input field is not marked as a mandatory field
 * #1686- Unable to use single quotes in the name input field of "Add new cmmand" modal
 * #1669- API calls are made automatically in a finished deployment details page
 * #1650- Duplicate log file paths could be entered in the Monitoring section of the Configure Page of device profiles
 * #1587- Breakage happens if an invalid Business Telemetry shell script is put in /opt/aikaan/scripts/telemetry dir
 * #1543- Ai agent uninstallation script should be provided for GoJek devices
## Known Caveats
 * #1918- The page reloads twice on post logging out
 * #1915- Maps doesnt load every single time when moving from one route back to dashboard
 * #1907- Unable to install the agent if wget is not present
 * #1897- Unable to use a single quote in the description input field of the devices
 * #1894- Business Events details displays error message
 * #1893- No "Device_Net_Reachable" event got logged for SysV device
 * #1881- Need a refresh button in syslog page of a device
 * #1880- The first universal search fails after every release
 * #1877- "Applications" sub-tab should be renamed
 * #1876- Apps/ Applications should be renamed as "Containers" in appropriate pages
 * #1875- Network Uptime and Device Uptime should be clearly specified

# **Version: 2.43.X**
  **Release: Production**
## Feature
 1. **Detailed Reports** <br>
     *Updated*: `Controller` <br>
     Users can generate reports for the Last hour / last day / last week or last month along with a custom time range. The reports could be grouped by 1 day / 1 Month. Users can click on rows to view the detailed report.
  2. **Limited Number of Auto retries** <br>
      *Updated*: `Controller` <br>
      Number of auto retry counts for deployment is limited to 5
  3. **Download deployment logs** <br>
      *Updated*: `Controller` <br>
      User can click on the *Download logs* button in the deployment details page on post clicking the *Logs* button.
  Note: The deployment status for each device is displayed without a percentage bar
## Bug Fixes
 * #1727- Abort button is absent in the Deployment details page while it is in the "Pending" state
 * #1726- Devices are not sorted correctly when the Device Profiles header is used in the Devices page
 * #1725- AND operator could be selected when Device Profile is chosen as a filter key
 * #1755- Incorrect "downlink" graph is displayed
 * #1752- Uplink graph is not displayed
 * #1751- Rephrase Graph names and Units
## Known Caveats
 * #1862- Rephrase "network_data_download" and "network_data_upload" column headers to "Download Data Consumption" and "Upload Data Consumption"
 * #1861- On post exporting the report, the device profiles and the reports disappears.
 * #1860- Device Uptime is lesser than the Network Uptime
 * #1858- Detailed report is not displayed for an offline device
 * #1857- Unable to type the report name in the input field
 * #1856- Duplicate hotspot events are logged
 * #1854- The Hotspot event's timestamp is incorrect
 * #1853- End time should not be less than start time
 * #1843- Incorrect status is displayed on post failure of first attempt in the deployment details page
 * #1842- UI/UX: No Proper loading message is displayed while opening the terminal of a device
 * #1840- Rephrase App deletion message
 * #1835- On clicking the "Abort" button in the deployment page, deployment details page is displayed
 * #1822- Exported Device Uptime report contains empty value for a powered off device
 * #1820- Rephrase suggestion message in the "Reports" page
 * #1819- All of a sudden login page gets displayed

# **Version: 2.41.X**
  **Release: Production**
## Feature
 1. **Filter Universal Search Result**\
     *Updated*: `Controller`\
     Users can streamline their search to only Devices/Device Profiles/Applications/Artifacts by clicking on the dropdown icon. By default *All* option is selected.
  2. **New Reports Added**\
      *Updated*: `Controller`\
      10 New reports have been added viz.
        * Process CPU Usage
        * Process Memory Usage
        * Process FD count
        * Process Thread count
        * Process data consumption (Upload)
        * Process data consumption (Download)
        * Network Speed (Upload)
        * Network Speed (Download)
        * Network Data (Upload)
        * Network Data (Download)
      The Processes and Network related reports will be generated for individual processes and interfaces respectively
## Bug Fixes
 * #1718- Unable to select 2 devices in first page of Devices
 * #1709- "Something Went Wrong" message is displayed if the filtered tags are removed in the Devices page
## Known Caveats
 * #1760- Universal Search doesn't display any result on post loading
 * #1755- Incorrect "downlink" graph is displayed
 * #1752- Uplink graph is not displayed
 * #1751- Rephrase Graph names and Units
 * #1750- The Version command overflows out of the Process card in the Monitoring page
 * #1746- Incorrect Network usage graph is displayed
 * #1744- Unable to add a log file which has a back slash in its name in the "Add new Log file" modal of "Monitoring" section
 * #1742- Incomplete Version is displayed in Reports page
 * #1741- 0% Process uptime is displayed, if an unmonitored process is selected

# **Version: 2.40.X**
  **Release: Production**
## Feature
 * **Device Filtering 2.0**\
    *Updated*: `Controller`\
    AND/OR operators could be selected beteen the filters and filter values. To do so, the user should click on the drop-down key of the operator and select the required one in the *Filter* modal.
## Bug Fixes
 * #1655- Deployment Progress Percentage should be truncated [*Updated*: `Controller`]
 * #1652- On clicking the Device Profiles name in the Deployments page, previously opened device profile's details page is displayed. [*Updated*: `Controller`]
 * #1643- Rephrase text in Reports page [*Updated*: `Controller`]
 * #1640- Abort button is not aligned properly in the Deployments page [*Updated*: `Controller`]
## Known Caveats
 * #1734- UX: Selecting/Adding a tag value in the device filtering modal is not user-friendly
 * #1733- Added filter values disappears in the "Device Filter" modal
 * #1730- Incorrect date is displayed in the x-axis of the graphs when "Last Month" is slected
 * #1727- Abort button is absent in the Deployment details page while it is in the "Pending" state
 * #1726- Devices are not sorted correctly when the Device Profiles header is used in the Devices page
 * #1725- AND operator could be selected when Device Profile is chosen as a filter key
 * #1720- On changing the port number, the "copy link" doesn't disappear 

# **Version: 2.39.X**
  **Release: Production**
## Feature
 1. **Auto-retry of Deployments**\
      *Updated*: `Controller` and `AiAgent`\
      The deployments will be retried on failed devices automatically. User should specify the number of retries while creating the deployment.
 2. **Health 2.0**\
      *Updated*: `Controller`\
      The Health page of devices has been updated with the *Bubble Chart*. The number of violations decides the health of the device.
## Bug Fixes
 * #1713- gopherry procstat causing other processes to crash [*Updated*: `Controller` and `AiAgent`]
 * #1590- Deployment filtering section includes the Offline command execution's artifacts as well.[*Updated*: `Controller`]
 * #1569- No suggestion message is displayed if user tries to create Device Profile/Dynamic Groups/Apps and devices with less than 4 characters in their names.[*Updated*: `Controller`]
 * #1530- Proper statement should be displayed while neglecting a process from Monitoring section.[*Updated*: `Controller`]
 * #1487- No suggestion message is displayed if blank space is entered as first or last character while filling up a form.[*Updated*: `Controller`]
## Known Caveats
 * #1689- User can enter special characters in the "Number of retries" input field
 * #1688- "Number of Retries" input field is not marked as a mandatory field
 * #1687- "Artifacts" sub-tab is displayed when clicked on the "Details" button present in the "Execute Command" section of a device profile
 * #1686- Unable to use single quotes in the name input field of "Add new cmmand" modal
 * #1680- Even though the application is deployed on the DP, "No Application deployed" message is displayed in android device
 * #1677- The execution of command fails on post executing a command which results in timeout error
 * #1670- Agent installation fails if the shell script is used on a raspberry pi after removing wget and curl
 * #1669- API calls are made automatically in a finished deployment details page

# **Version: 2.38.X**
  **Release: Production**
## Feature
 1. **Telemetry Cache Enabled**\
      *Updated*: `Controller` and `AiAgent`\
      Entire telemetry data will be stored during the offline period and will be synced with the cloud once the internet connectivity is enabled for the device.
 2. **Role Based Access Updated**\
      *Updated*: `Controller`\
      Only Admin can create, update and delete users.
## Bug Fixes
 The following bugs have been fixed.
  * #1591- No input field is displayed in the filtering section on post Clicking the Reset filters and refresh button.[*Updated*: `Controller`]
  * #1588- Blank page is displayed if tried to open the terminal of an android device.[*Updated*: `Controller`]
  * #1551- When a user with manager role tries to add a process to monitor, error message is displayed but the Process name is added.[*Updated*: `Controller`]
 * #1546- "Manager" should be renamed as "Reporter".[*Updated*: `Controller`]
 * #1533- Report section keeps on buffering when tried to generate the report for large number of devices.[*Updated*: `Controller`]
 * #1494- Detailed graphs of detected Binary apps are not displayed.[*Updated*: `Controller`]
## Known Caveats
* #1655- Deployment Progress Percentage should be truncated
* #1654- On post clicking the "Reset filters and Refresh" button, unable to select the previously selected filter keys in deployments page
* #1652- On clicking the Device Profiles name in the Deployments page, previously opened device profile's details page is displayed
* #1651- Dynamic Group Creation modal gets closed on clicking the close icon of feedback error message
* #1650- Duplicate log file paths could be entered in the Monitoring section of the Configure Page of device profiles
* #1643- Rephrase text in Reports page
* #1640- Abort button is not aligned properly in the Deployments page
* #1638- Provisioned application restarts automatically if the device's internet connectivity is turned On

# **Version: 2.36.X**
  **Release: Production**
## Feature
 1. **Device Clustering**\
    *Updated*: `Controller`\
    Devices with similar hardware and events are grouped together and displayed On the device's Info/Events page. Along with the names, the status of the devices is also displayed.
 2. **Error log Notification** (alpha)\
    *Updated*: `Controller` and `AiAgent`\
    A new event gets registered if more than 40 error messages are present in the monitored log file within 4 hours.
 3. **Auto removal of cached artifact files**\
    *Updated*: `Controller` and `AiAgent`\
    The cached files which get downloaded during the *OTA Upgrade* process will be removed automatically, thus freeing up space.
## Note:
 * Users are should provide at least 4 characters (without blank spaces) at the time of creation/updation of device/device profiles/dynamic groups/application names.
## Known Caveats:
 * #1591- No input field is displayed in the filtering section on post Clicking the Reset filters and refresh button
 * #1590- Deployment filtering section includes the Offline command execution's artifacts as well
 * #1588- Blank page is displayed if tried to open the terminal of an android device
 * #1587- Breakage happens if an invalid Business Telemetry shell script is put in /opt/aikaan/scripts/telemetry dir
 * #1574- log file names are not displayed properly in the drop-down list
 * #1569- No suggestion message is displayed if user tries to create Device Profile/Dynamic Groups/Apps and devices with less than 4 characters in their names
 * #1439- Common feedback infrastructure is not used in change password page

# **Version: 2.35.X**
  **Release: Production**
## Feature
 1. **Agent for Android devices**\
   *Updated*: `Controller` and `AiAgent`\
    Users can install the agent on the android devices. Users can download the script file by selecting the include dependencies checkbox in the *Advanced options*section of the *Device Profile*'s details page.
 2. **Consistent Filtering section**\
   *Updated*: `Controller`\
   The filtering operation is made consistent in the Devices, Reports, Events, and in OTA_Upgrade pages.
## Note:
 * RBAC policies have been updated from the current version.
## Known Caveats
 * #1551- When a user with manager role tries to add a process to monitor, error message is displayed but the Process name is added
 * #1546- "Manager" should be renamed as "Reporter"
 * #1543- Ai agent uninstallation script should be provided for GoJek devices
 * #1539- Process name is not displayed in the exported csv file
 * #1533- Report section keeps on buffering when tried to generate the report for large number of devices
 * #1530- Proper statement should be displayed while neglecting a process from Monitoring section
 * #1529- Unable to edit the Processes

# **Version: 2.34.X**
  **Release: Internal**
## Feature
 1. **Offline Command Execution**\
  *Updated*: `Controller` and `AiAgent`\
  Users can trigger commands for execution on a group of devices present in a device profile, irrespective of the status of the devices (offline/ online). Follow the below steps to trigger a command execution:
    1. Click on the *Device Profiles* navigation tab
    2. Click on a device profile (in which the Device Commands are present in the *Configure* section)
    3. Click on the drop-down icon present in the *Execute Command* section
    4. Select command and click on the *Execute* button
  Users can check the status of the command execution by clicking on the *Details* button.
 2. **Export Reports as CSV files**\
    *Updated*: `Controller`\
    The generated reports can be exported as CSV files in the *Reports* page
## Known Caveats
 * #1509- On post clicking the "Execute" button, the Command Selection input field is not cleared.
 * #1508- Unable to cancel the recently Executed Command
 * #1507- Incorrect error message is displayed if user tries to execute a command in a device profile which has no devices
 * #1505- "Updating to" section should not be displayed in the details page of Command Execution
 * #1494- Detailed graphs of detected Binary apps are not displayed
 * #1487- No suggestion message is displayed if blank space is entered as first or last character while filling up a form

# **Version: 2.33.X**
  **Release: Production**
## Feature
 1. **Universal Search**\
  *Updated*: `Controller`\
  Users can search for devices, device profiles, applications and artifacts by entering their names in the search bar present on the dashboard. Users can also navigate to the searched device's/device profile's info page by clicking the search result.
 2. **Device Profiles in OTA Upgrade**\
 *Updated*: `Controller`\
  From this release onwards, deployments could be done on device profiles as well. There is slight variation in filling up the deployment creation form where the user should select *Device Profile* or *Dynamic Group* from the *Deploy On* dropdown list and then select a respective device profile/dynamic group in the next input field
## Known Caveats
 Universal Search-
  * Incorrect order of search result is displayed
  * While the search operation is going on, No results found message is displayed
  * Incorrect search result is displayed on post entering one character for the second time

OTA Upgrade-
  * Improper message is displayed on post deleting the device profile and clicking the "Retry Deployment" button
  * On post deleting a device profile, the type column doesn't give any information
  * Clicking on "Reset Filters and refresh" button in deployments page doesn't reset "Device Profile"

Other Issues-
  * On clicking the name of a Dynamic group "No Device present" message is displayed first and then the devices are displayed
  * Common feedback infrastructure is not used in change password page
  * Neighbouring device's names are not displayed in Neighbours page
  * Unable to edit/ delete commands if a command is created with more than 100 characters 

# **Version: 2.32.X**
  **Release: Production**
## Feature
 1. **What's New**: Users can get to know about the latest features by clicking on the *What's new* button. The button vanishes once the user has gone through the release notes. However, he can recheck it by clicking on the *User* navigation tab and then clicking on the *What's new* option.
 2. **Live event notifier**: User can enable/disable the event notifier on a particular PC. This can be done on the user settings page. Follow the steps to enable the notifier. 
    1. Click on the *User* navigation tab and then click on the *Settings* option
    2. Click on the *Notifications* sub-tab
    3. Click on the event's toggle switch
    Note: By default, the live event notifier will be turned off. Hence you won't see any pop-up events at the bottom right corner.
 3. **Auto deployment of artifacts**: Users can configure a Device Profile (Device Groups) in such a way that whenever a new device gets created (/registered) in a device profile, it would get the specified artifacts. Following steps describe the way to create auto deployments
    1. Click on the *Device Profiles* navigation tab
    2. Click on a device profile and then click on the *Configure* sub-tab
    3. Click on the *Select artifacts* input field and then select the artifacts
    4. Click on the *Save* button
    Note: On post performing the above steps, the selected artifacts will be deployed on the newly created devices.
 4. **CPU/Memory/Disk Usage Reports**: Users can generate CPU/Memory/Disk Usage reports. Users can also get to know how many docker error logs have been generated on a particular day. Following are the steps to generate the report
    1. Click on the *Reports* navigation tab
    2. Click on the *Select* button and choose profiles/groups
    3. Click on the next *Select* button and select the CPU/Memory/Disk usage filters by clicking on the *Add another filter* button each time
    4. Click on the *Generate report* button
 5. **New Business Event API**: Users can make usage of the new cloud API to trigger a business event. Click [here](https://api.aikaan.io/?urls.primaryName=EventService) to know more about the API.
 NOTE: *Device Groups* has been renamed as **Device Profiles** and *Custom Groups* has been renamed as **Dynamic Groups**.
## Known Caveats
 None

# **Version: 2.31.X**
  **Release: Production**
## Feature:
 1. **Execute Commands**: The commands could be executed on the devices with few clicks. The following steps will guide you to add commands.
    1. Click on the *Device Groups* navigation tab and then click on a device group
    2. Click on *Configure* sub-tab and Click on the *Add new command* button.
    3. Fill-up the fields and click on the *Add* button.

    On successfully adding the commands, the user can execute them on the devices present in that device group. To execute the commands follow the below steps:
    1. Click on the *Devices* navigation tab and click on the device which belongs to the previously used device group.
    2. Click on the *Execute command* button
    3. Select a command by clicking on it and then click on the *Execute* button
  2. **Roles/ABAC**: Users should be specified with the roles to access devices and groups with certain permissions.
     * *Roles and Permissions*:  

         |   Roles  |      Permissions     |
         |:--------:|:--------------------:|
         |  Manager |         Read         |
         | Operator |      Read, Write     |
         |   Admin  | Read, Write, Execute |
 
     At present, any user with the role *operator* or *admin* can perform *Multiple Device Migration* operation, while the user with the role *Manager* can't do it since he has only *Read* permission. On subsequent releases few more operations specific to roles will be updated.
## Known Caveats:
  None

# **Version: 2.30.X**
  **Release: Production**
## Feature:
  1. **Process Up-time Report**: Monitored Processes' up-time report could be generated. Users can navigate to Reports page and select process names and get a report of the monitored processes. The generated process uptime values are calculated from 2.30.X release date.
  2. **Update Users and Departments**: The user names and departments present in the Settings page could be edited. User can change the name, description, department and its type by clicking the edit icon.
  3. **Department Scope**: Users can be restricted from accessing all the Device groups by specifying the scope to departments. To do so, go to the user settings page, click on the Scope button present against a department, and select device groups. Now the users who are present in the specific department can have access only to the specified device groups and devices. 
  3. **Upgrade in Event Filters**: A new approach has been introduced in the event filtering mechanism. Users need to fill-up the form to get an accurate list of events. At present only AND operator exists between multiple filters and OR operator exists between multiple filter values.
## NOTE:
  * If none of the device gruops is specified as the scope of the Department, then the users of the department can have access to all the device groups present in the organization.
  * A device group can be specified as a scope to only one department.
## Known Caveats:
  None

# **Version: 2.29.X**
  **Release: Production**
## Feature:
  1. **Multiple Device Migration**: Multiple devices could be migrated from one device group to another using a .csv file. To do so, create csv file with a list of device names and the destination device group names. Click on the *username* navigatoin tab present at the top right corner of the page and then click on the *Settings* option. Click on the *Device Management* sub-tab and upload the csv file. Click on the *Start migrating* button. In a few seconds devices will be moved to the respective device groups specified in the csv file.
  2. **Device Uptime Report**: Now the user can get Device Uptime report along with the network uptime. Follow the steps to get the report:  
    a) Click on the *Reports* navigation tab  
    b) Click on the *Select* button present against the *Select Devices From*  
    c) Choose custom groups and/or device groups to select a set of devices  
    d) Click on the Select button and choose *Network uptime* and/or *Device Uptime*  
    e) Click on the *Generate Report* button  
## Known Caveats:
  None

# **Version: 2.28.X**
  **Release: Internal**
## Feature:
  1. **Create Multiple users**: Multiple users could be created using a .csv file. To do so create a .csv file containing emailID,username and Role. Ex: test@mail.io,TestName,TestEng. Now login to Aikaan dashboard, click on the *username* navigation tab present at the top right corner of the page and then click on the *Settings* option. Click on the *Create New Users* button and select *Multiple Users* from the *Create* dropdown list. Click on the *Next* button and upload the csv file and click on the create button.
  2. **Date Picker for Detected Apps**: Users can click on the Date picker present in the Detected apps page. Users can get to know about which were all the apps/ dockers were running on a specific day at a specified time interval.
## NOTE:
  The Bleeding device's notifications have been disabled on the dashboard. However the events will be sent over slack channels and even could be seen in the Events page.
## Known Caveats:
  None

# **Version: 2.27.X**
  **Release: Production**
## Feature:
  1. **Configurable Event Reception time**: The *telemetry* frequency could be configured according to the requirement. All the Health and Anomaly related calculations will be done according to the configured frequency.
  2. **Improved Events**: All the events related to device reachability will be received with a proper timestamp. The time in which the device was powered up will be captured as an event. The Device Reachable/Unreachable event will be received based on the configured reception time.
  3. **Offline Metric data collection**: Even if a device is not connected to the internet, it's business metrics related data will still be captured and once the device is connected back to the internet it will be reflected on the *Visualization* page of the device.
  4. **Process crash Indication**: User gets a pop-up error message when a monitored process crashes or restarts.
  5. **Deployment failure logs**: The logs of the failed deployments could be seen by clicking the *Logs* button present in the deployment details page. The *Logs* button appears if the deployment fails for a particular device.
## NOTE:
  The time in which the device got created will be displayed on the Devices page as well.
## Known Caveats:
  None.

# **Version: 2.25.1**
  **Release: Production**
## Feature:
  1. **Report Generation**: Users can generate reports of a group of devices. The report includes *Network Uptime* and *Device Uptime*. To generate the report, users should first apply the filters in the *Filter* section and then he should click on the *Generate Report* button.
  2. **Business Metrics**: Users of an organization can view business metrics of devices in the device's *Visualization* sub-tab. To view the metrics, write a shell script in ```/opt/aikaan/scripts/telemetry``` directory. Within a few minutes, users can select the Business telemetry drop-down option in the last section of the visualization page and observe the metric. Even multiple business metrices could be visualized.
## Note:
  Only the Fatal and Error events will pop up on the dashboard and not all the events. However all the events will be accumulated in the *Events* page.
## Known Caveats:
  None.

# **Version: 2.24.1**
  **Release: Production**
## Feature:
  1. **Network speed graph**:  Upload speed and download speed for a device could be visualized graphically.
  2. **Device Creation Time**: The creation time of a device is now displayed in the device's info section.
  3. **Process Monitoring**: Users can monitor multiple processes, running in a group of devices. Go to the Device Groups page and click on a device group. Now click on the *Configure* sub-tab and enter the name of the process(s). Click on the *Add* and *Update* button. Now you can check the process details in the *Applications* sub-tab of a device, under the *Process Monitoring* section.
  4. **Logs Monitoring**: Any log file, present in a device could be viewed on the browser. To do so, enter the log file name along with its path in the *Configure* section of the device group and select the file in the *System Logs* sub-tab of a device, which belongs to the same device group.
  5. **Binary Apps Info**: The CPU and Memory usage information of the  detected binary apps is displayed in percentage in the *Applications* page of a device, under the *Detected Binary apps* section.
## Known Caveats:
  None  

# **Version: 2.23.1**
  **Release: Production**
## Feature:
  **Terminal on Browser**: Any device's terminal could be accessed from the browser itself.  
  This feature is supported for all the devices in which the non-docker Aiagents are running.
## Known Caveats:
  None

# **Version: 2.22.1**
  **Release: Internal**
  1. In this release, minute changes are made to the Events page where *Load More* button is added. Users can click on it to see more events.
  2. Bugs related to *Forgot Password* and *Open Port* are fixed.
  3. The Uptime of devices that are offline for more than 30 days gets updated.
  4. The *Peripheral* and *Network Traffic* sub-tabs remain highlighted even if the device is offline.
## Known Caveats:
  None

# **Version: 2.21.1**
  **Release: Production**
## Features:
  1. **Improved Custom Group Creation Flow**: New modal for creation of custom group.
  2. **Device-Artifact Compatibility Information**: Number of devices selected for a deployment is displayed before the creation of deployment. 
  3. **Search/Sort/Filters in Deployments page**: Users can search for a deployment using the Deployment names. The deployments can also be sorted and filtered based on the user who initiated the deployment, Artifacts, Start date and Status of deployments.
  4. **System Tags**: Write a shell script file and save it in ```/opt/aikaan/etc/support/inventory/``` directory with the file name starting with *mender-inventory-*. The contents of the file should have key-value pairs. These key-values appear as tags on the AiCon Dashboard. Ex: CPU = 4 Cores, RAM = 16GB, etc. These tags could be used to filter the devices and to create custom groups.
  5. **System-wide Feedback**: A common System-wide feedback infrastructure has been implemented and is being used in the OTA Upgrade page and in Custom Groups page.
## Known Caveats:
  None

# **Version: 2.20.1**
  **Release: Production**
## Features:
  1. **Custom Groups 2.0**: You can now create your own dynamic groups apart from static device groups. A device can be a part of one or more Custom groups. You can group devices based on system architectures, Location, number of CPU cores, etc. Groups can be formed based on one or more tags.
  2. **Tags to Devices**: Ability to add your own tags to one or more devices which can be used for grouping.
  3. **OTA Upgrade**: You can now perform OTA upgrades on one or more devices. These can be system updates, application updates, config updates, command execution, etc.
## Known Caveats:
  None

# **Version: 2.17.1**
  **Release: Internal**
## Features:
  1. **Apps Section UI Revamp**: The UI of Apps page has been revamped by replacing the App cards to a table to be more consistent with the rest of the UI interface
  2. **Neighbors Section Enhancements**: The Neighbors section has been enhanced to support IP sorting of entries, better UX to recognize Aikaan agents in the network etc.
  3. **OpenID Role/User Support**: AiCon now supports openID based authentication for the extended users you create in the settings Page. The user can choose whether the extended users can be authenticated normally via email/password or via openID authentication.
## Known Caveats:
  None

# **Version: 2.16.1**
  **Release: Production**
## Features:
  1. **Social ID Login support**: AiCon now supports logging in via OAuth from providers like Azure & Google. Use your Azure/Google Credentials to authorize yourself into the AiCon system. This feature currently works only if your account is pre-authorized by Aikaan.
	2. **Actionable Alerts**: All alerts in the Events page as well as slack will now contain hyperlinks which will take you to the most relevant page of the alert. For example: If you have a CPU High alert then you will be taken to the Visualizations page which has the CPU Graph. If you have a application error logs alert then you would be taken to the page which shows the error logs for that particular application.
	3. **AiCon Release Info**: You can now view information on what version of AiCon you are running. Click on your account on the top right corner and click on About to see which version of AiCon you are running.
## Known Caveats:
  None

# **Version: 2.15.1**
  **Release: Internal**
  This is an internal release with minute modifications in *System Logs* page.

# **Verson: 2.14.1**
  **Release: Production**
## Features
  1. **Forgot Password**: Ability to reset your password incase you've forgotten it. Simply type your email in the forgot password page and you will receive a reset link in your email where you'd have the option to type a new password.
  2. **Improved System/Docker/Binary Logs**: New and improved mechanism to view logs:
    a.  **Search support** - search within the logs for a particular text.
    b.  **Download support** - download logs as a file.
    c.  **Time range support** - Get logs for a particular time duration.
    d.  **Log filter** - Filter logs by output and error type.
  3. **Notify Boolean Support for Business Events**: We've introduced a new *notify* flag as part of the AiAgent API to send a business event. If *notify* is ```TRUE```, then your event would be sent to slack as well as popup on your browser apart from being present in the events page, else it would only show up in the events page. If the *notify* flag is not set, then by default notify is ```TRUE```.
  4. **Mender to Mainline**: We have brought the entire mender stack to mainline which means all AiCon deployments will automatically get mender support.
  5. **Agent Info API**: We have introduced a new API on AiAgent which would return the info of the agent. For example: Device ID, Profile ID.
  6. **Customer Specific UI Theme at runtime**: We have made it possible for customers to have their own UI themes which would change the logo,titles,button colors etc all of which at runtime based on certain environment variables. This was earlier possible at compile time which was cumbersome as it required multiple images to be built.
## Known Caveats:
  None
