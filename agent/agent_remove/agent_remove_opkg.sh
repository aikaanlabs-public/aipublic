#!/bin/sh
# This script cleans up an aikaan agent installed in a device
#

set -ev

user="$(id -un 2>/dev/null || true)"

if [ "$user" != 'root' ]; then
        echo "Should be root user to run the script"
        exit 1
fi

REMOVE_DIR=$INSTALL_DIR/opt/aikaan
# Check if INSTALL_DIR is set using ENV or as parameter
if [  -z $INSTALL_DIR ]; then
    INSTALL_DIR=$1
fi

# For custom path, an aikaan directory get created
if [ ! -d $INSTALL_DIR/opt/aikaan ]; then
    if [ -d $INSTALL_DIR/aikaan/opt/aikaan ]; then
        INSTALL_DIR=$INSTALL_DIR/aikaan
        REMOVE_DIR=$INSTALL_DIR
    else
        echo "Error: Couldn't find $INSTALL_DIR/opt/aikaan directory ..."
        echo "The device is already cleaned up Or installation path is incorrect ..."
        exit 2
    fi
fi

$INSTALL_DIR/opt/aikaan/bin/opkg -f $INSTALL_DIR/opt/aikaan/etc/opkg.conf remove aiagent
$INSTALL_DIR/opt/aikaan/bin/opkg -f $INSTALL_DIR/opt/aikaan/etc/opkg.conf clean

SYSMANVAL=$(ls -l /proc/1/exe)
case $SYSMANVAL in
    *systemd)
        echo "system manager: systemd"
        systemctl stop aikaan-agent-opkg-updater.service
        systemctl disable aikaan-agent-opkg-updater.service
        systemctl stop aikaan-agent-opkg-updater.timer
        systemctl disable aikaan-agent-opkg-updater.timer
        systemctl stop aikaan.service
        systemctl disable aikaan.service
        rm -rf /etc/systemd/system/aikaan*
        rm -f /usr/local/var/lib/opkg/info/aiagent*
        rm -f /usr/local/var/lib/opkg/status
        ;;
    *init)
        echo "Deleting files: $(ls -l /etc/init.d/*aikaan*)"
        rm -rf /etc/init.d/*aikaan*
        ;;
    *busybox)
        if [ -f /usr/local/etc/init.d/S99aikaan ]; then
            echo "Deleting files: $(ls -l /usr/local/etc/init.d/*aikaan*)"
            /usr/local/etc/init.d/S99aikaan stop
            /usr/local/etc/init.d/S99aikaan-agent-opkg-updater stop
            rm -rf /usr/local/etc/init.d/*aikaan*
        elif [ -f /etc/init.d/S99aikaan ]; then
            echo "Deleting files: $(ls -l /etc/init.d/*aikaan*)"
            /etc/init.d/S99aikaan stop
            /etc/init.d/S99aikaan-agent-opkg-updater stop
            rm -rf /etc/init.d/*aikaan*
        fi
        ;;
    *)
        echo "system manager: $SYSMAN is not supported"
        ;;
esac

rm -rf $REMOVE_DIR

echo "aiagent is uninstalled and the directories are clean"
