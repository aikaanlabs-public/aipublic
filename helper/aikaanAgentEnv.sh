#!/bin/sh
BIKE_ID_FILE=/userdata/bike_id
AIKAAN_AGENT_ENV_FILE=/etc/aikaanAgentEnv.json
if [ -f "$BIKE_ID_FILE" ]; then
	bike_id=`cat /userdata/bike_id`
	echo {\"deviceName\":\"$bike_id\"}
elif [ -f "$AIKAAN_AGENT_ENV_FILE" ]; then
	cat $AIKAAN_AGENT_ENV_FILE
fi
